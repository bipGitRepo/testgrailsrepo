package testgrailsproject


class User {
    String title;
    Date lastVisitTime;
    int age;
    String phone;

    static constraints = {
        title blank: false, nullable: false
        lastVisitTime nullable: false
        age min: 10
        phone nullable: false, minSize: 13, maxSize: 16
    }
}
