import testgrailsproject.User

class BootStrap {

    def init = { servletContext ->
        new User(title: "firstUser", lastVisitTime: new Date(), age: 22, phone: "+380662535724").save()
        new User(title: "secondUser", lastVisitTime: new Date()+234, age: 33, phone: "+380501111111").save()
    }
    def destroy = {
    }
}
