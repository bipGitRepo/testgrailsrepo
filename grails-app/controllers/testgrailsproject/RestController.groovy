package testgrailsproject

import grails.rest.RestfulController

class RestController extends RestfulController {
    static responseFormats = ['json', 'xml']

    RestController() {
        super(User)
    }

    def index() { }

}
